Feature: Buscar hoteles con l tarifa mas economica
  y validar que el precio a pagar sea correcto.

  Scenario Outline: Buscar la tarifa mas economica en hoteles
    Given ingreso a la pagina de agencia de viajes
    When busco hoteles con los filtros de <Location>, <CheckIn>, <CheckOut>.
    And selecciono la opcion mas economica
    Then se valida que el precio sea el correcto segun calculo
    Examples:
    |Location|CheckIn|CheckOut|
    |Honolulu|2/25/2021|2/27/2021|
    |Las Vegas|2/25/2021|2/27/2021|