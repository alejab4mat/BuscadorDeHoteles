package stepsDefinitions;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;
import questions.ElPrecio;
import tasks.Buscar;
import tasks.Seleccionar;
import userinterfaces.PaginaDXHotelsUserInterface;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class BuscarHotelStepsDefinition {

    @Managed(driver="chrome")
    private WebDriver navegadorChrome;

    @Before
    public void configuracionInicial() {
        OnStage.setTheStage(new OnlineCast());
        theActorCalled("user").can(BrowseTheWeb.with(navegadorChrome));
    }

    @Given("^ingreso a la pagina de agencia de viajes$")
    public void ingreso_a_la_pagina_de_agencia_de_viajes() {
        theActorInTheSpotlight().wasAbleTo(Open.browserOn(new PaginaDXHotelsUserInterface()));
    }

    @When("^busco hoteles con los filtros de (.*), (.*), (.*).$")
    public void busco_hoteles_con_los_filtros_de(String location, String checkIn, String checkOut) {
        theActorInTheSpotlight().attemptsTo(Buscar.hotelesConFiltros(location,checkIn,checkOut));
    }

    @When("^selecciono la opcion mas economica$")
    public void selecciono_la_opcion_mas_economica() {
        theActorInTheSpotlight().attemptsTo(Seleccionar.laOpcionMasEconomica());
    }

    @Then("^se valida que el precio sea el correcto segun calculo$")
    public void se_valida_que_el_precio_sea_el_correcto_segun_calculo() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(ElPrecio.totalCorrecto()));
    }

}
