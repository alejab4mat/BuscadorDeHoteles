package tasks;

import interactions.Desplegar;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.openqa.selenium.Keys;
import userinterfaces.PaginaDXHotelsUserInterface;

import static userinterfaces.PaginaDXHotelsUserInterface.*;

public class Buscar implements Task {
    String location;
    String checkIn;
    String checkOut;
    public Buscar(String location, String checkIn, String checkOut) {
        this.location = location;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
    }
    public static Buscar hotelesConFiltros(String location, String checkIn, String checkOut) {
        return Tasks.instrumented(Buscar.class,location,checkIn,checkOut);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(UBICACION),
                Desplegar.listaDeLocacionesYSeleccionar(location),
                Enter.theValue(checkIn).into(FECHA_INGRESO).thenHit(Keys.ENTER),
                Enter.theValue(checkOut).into(FECHA_SALIDA).thenHit(Keys.ENTER),
                Click.on(BOTON_BUSCAR)
                );
    }
}
