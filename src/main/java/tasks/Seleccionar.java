package tasks;

import interactions.Elegir;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

public class Seleccionar implements Task {
    public static Seleccionar laOpcionMasEconomica() {
        return Tasks.instrumented(Seleccionar.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Elegir.laOpcionConElMenorPrecio());
    }
}
