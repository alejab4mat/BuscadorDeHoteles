package interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Tasks;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import userinterfaces.PaginaDXHotelsUserInterface;

import java.util.List;

public class Desplegar implements Interaction {
    String location;

    public Desplegar(String location) {
        this.location = location;
    }

    public static Desplegar listaDeLocacionesYSeleccionar(String location) {
        return Tasks.instrumented(Desplegar.class,location);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        List<WebElement> listOfStates = PaginaDXHotelsUserInterface.LISTA_DE_UBICACIONES.resolveFor(actor).findElements(By.tagName("div"));
        for (int i = 0; i < listOfStates.size(); i++) {
            if (location.equals(listOfStates.get(i).getText())) {
                listOfStates.get(i).click();
                break;
            }
        }

    }
}
