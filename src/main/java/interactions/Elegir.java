package interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import userinterfaces.PaginaDXHotelsUserInterface;

import java.util.ArrayList;
import java.util.List;

public class Elegir implements Task {
    public static Elegir laOpcionConElMenorPrecio() {
        return Tasks.instrumented(Elegir.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        int menor = 0;
        List<Integer> listaPrecios = new ArrayList<>();
        List<WebElement> listOfPrices = PaginaDXHotelsUserInterface.LISTA_DE_PRECIOS.resolveFor(actor).findElements(By.xpath("//*[@class='rate-number']"));
        List<WebElement> listOfButtons = PaginaDXHotelsUserInterface.LISTA_DE_PRECIOS.resolveFor(actor).findElements(By.xpath("//span[contains(text(), 'Book it')]"));
        for(int k=0; k<3; k++ ) {
            listaPrecios.add(Integer.parseInt(listOfPrices.get(k).getText().replace("$","")));
        }
        Integer auxiliar = listaPrecios.get(0);
        for (int i = 0; i < listaPrecios.size(); i++) {
            if (listaPrecios.get(i) < auxiliar){
                auxiliar= listaPrecios.get(i);
                menor=i;
            }
        }
        listOfButtons.get(menor).click();

    }
}
