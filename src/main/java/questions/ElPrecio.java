package questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import userinterfaces.PaginaDXHotelsUserInterface;

import java.util.ArrayList;
import java.util.List;

import static userinterfaces.PaginaDXHotelsUserInterface.PRECIO_FINAL;

public class ElPrecio implements Question<Boolean> {
    public static ElPrecio totalCorrecto() {
        return new ElPrecio();
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        Boolean resultado = true;
        double total = 0.0;
        List<Double> listaPrecios = new ArrayList<>();
        List<WebElement> listOfPrices = PaginaDXHotelsUserInterface.PRECIO_NOCHE.resolveFor(actor).findElements(By.xpath("//h4[@class='price']"));

        for(int k=0; k<listOfPrices.size(); k++ ) {
            listaPrecios.add(Double.parseDouble(listOfPrices.get(k).getText().replace("$","")));
        }
        for (int i = 0; i < listaPrecios.size(); i++) {
            total = total + listaPrecios.get(i);
        }
        if (total == Double.parseDouble(Text.of(PRECIO_FINAL).viewedBy(actor).asString().replace("$",""))){
            resultado = true;
        } else {
            resultado = false;
        }
        return resultado;
    }
}
