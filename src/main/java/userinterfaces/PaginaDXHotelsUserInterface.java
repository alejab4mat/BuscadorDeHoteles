package userinterfaces;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

@DefaultUrl("https://js.devexpress.com/Demos/DXHotels/#home")
public class PaginaDXHotelsUserInterface extends PageObject {
    public static final Target UBICACION = Target.the("lista desplegable").
            located(By.xpath("(//input[@role='combobox'])[last()-5]"));
    public static final Target LISTA_DE_UBICACIONES = Target.the("Lista de ciudades").
            located(By.xpath("//div[@class='dx-scrollable-wrapper']/div/div[1]"));
    public static final Target FECHA_INGRESO = Target.the("Fecha de ingreso").
            located(By.xpath("(//input[@role='combobox'])[last()-4]"));
    public static final Target FECHA_SALIDA = Target.the("Fecha de salida").
            located(By.xpath("(//input[@role='combobox'])[last()-3]"));
    public static final Target BOTON_BUSCAR = Target.the("Boton de buscar").
            located(By.className("button-search"));
    public static final Target LISTA_DE_PRECIOS = Target.the("Lista de precios").
            located(By.className("current-hotels"));
    public static final Target PRECIO_NOCHE = Target.the("Precios por noche").
            located(By.className("total-pay"));
    public static final Target PRECIO_FINAL = Target.the("Precio final mostrado").
            located(By.xpath("//h4[@class='total-price']"));
}
